const form = document.getElementById('formularioCliente');
const mensaje = document.getElementById('mensaje');

form.addEventListener('submit', function (e) {
    e.preventDefault();
    const nombre = document.getElementById('nombre').value;
    const apellido = document.getElementById('apellido').value;
    const email = document.getElementById('email').value;
    const telefono = document.getElementById('telefono').value;

    mensaje.innerHTML = `Cliente registrado: ${nombre} ${apellido}, Email: ${email}, Teléfono: ${telefono}`;
    form.reset();
});
